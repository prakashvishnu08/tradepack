Rails.application.routes.draw do

  devise_for :users, controllers: {registrations: 'users/registrations'}, path: 'users'
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
    get '/users' => 'devise/registrations#new'
  end
  devise_for :admins, :skip => [:registrations], path: 'admins'
  as :admin do
    get 'admins/edit' => 'devise/registrations#edit', :as => 'edit_admin_registration'
    put 'admins' => 'devise/registrations#update', :as => 'admin_registration'
  end

  namespace :admin do
    resources :sellers, only:[:new, :index, :create, :edit, :update, :destroy, :show] do
      collection do
        get 'states'
        get 'load_size_values'
      end
    end
    resources :pallets, only: [:update]
    resources :categories, only:[:new, :index, :create, :edit, :update, :destroy] do
      collection do
        get "load_size_types"
      end
    end
    resources :dashboard, only:[:index]
    #resources :products, only:[:index, :new, :create, :edit, :update, :show]
    resources :option_types, only:[:index, :new, :create, :edit, :update]
    resources :users, only: [:index, :show] do
      member do
        get 'approve'
      end
    end
    resources :orders, only: [:index, :show, :update] do
      member do
        get 'cancel_order'
        get 'paid_order'
        get 'change_delivery_status'
      end
      collection do
        get 'public_orders', as: "public"
      end
    end
    root to: "categories#index"
    # get 'sellers/states'
  end
  resource :user, only: [:edit, :update] do
    collection do
      get "new_address"
      post "create_address"
      get "edit_address"
      patch "update_address"
      get 'delete_address'
    end
  end
  resources :notifications, only:[:index]
  resources :orders, only: [:new, :create] do
    collection do
      get 'select_category'
      get 'my_requests'
      get 'my_orders'
      post "hook"
      get 'order_summary'
      get 'order_canceled'
      get 'get_pallet'
      get 'ask_with_comparator'
    end
    member do
      get 'cancel_order', as: "cancel"
      get 'invoice'
      get 'offer_summary'
    end
  end
  get '/orders', to: 'orders#my_requests'
  get 'categories/index'
  get 'categories/states'
  get '/home/contact_us'
  get '/home/privacy'
  get '/home/faq'
  get '/home/how_it_works'
  get '/home/home_page'
  get '/home/comparator'
  get '/home/get_sellers'
  post '/home/send_message'

  root to: "home#home_page"
  match '*path', via: :all, to: redirect('/404')

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
