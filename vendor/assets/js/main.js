
/* file:           plugins.js
 * version:        1.3
 * last update:   23.03.2014
 * description:    All template plugins included in this file
*/

$(document).ready(function(){

    /* page toolbar tabs */
    $(document.body).on("click",".page-toolbar-tabs li a",function(){
        var pli = $(this).parent("li");
        var act = $($(this).attr("href"));

        var parentTab = $(this).parents(".page-toolbar-tab");

        if(parentTab.length > 0){
            parentTab.find(".page-toolbar-tabs li").removeClass("active");
            parentTab.find(".page-toolbar-tab").removeClass("active");
        }else{
            $(".page-toolbar-tabs li,.page-toolbar-tab").removeClass("active");
        }


        pli.addClass("active");
        act.addClass("active");

        return false;
    });
    /* eof page toolbar tabs */

    /* daterangepicker */
    gDateRangePicker = {
        init: function(container){
            if($(container+" .daterange").length > 0)
               $(container+" .daterange").daterangepicker({format: 'YYYY-MM-DD',startDate: '2013-01-01',endDate: '2013-12-31'});
        }
    }
    /* eof daterangepicker */

    /* Summernote */
    gSummernote = {
        init: function(container){
            if($(container+" .editor").length > 0)
               $(container+" .editor").summernote({height: 300});
        }
    }
    gSummernoteEditor = {
        init: function(container){
            $(container+" .email_editor").on("click",function(){
                $(container+" .email_editor").summernote({height: 300,focus: true});
            });
        }
    }
    /* eof Summernote */

    /* Fancybox */
    gFancybox = {
        init: function(container){
            if($(container+" .fancybox").length > 0)
               $(container+" .fancybox").fancybox({padding: 5});
        }
    }
    /* EOF Fancybox */

    /* jQuery */
        /* datepicker */
        gDatepicker = {
            init: function(container){
                if($(container+" .datepicker").length > 0)
                   $(container+" .datepicker").datepicker({nextText: "", prevText: "",showOtherMonths: true, selectOtherMonths: true, dateFormat: "yy-mm-dd"});
            }
        }
        gMDatepicker = {
            init: function(container){
                if($(container+" .mdatepicker").length > 0)
                   $(container+" .mdatepicker").datepicker({nextText: "", prevText: "", numberOfMonths: 3, dateFormat: "yy-mm-dd"});
            }
        }
        /* eof datepicker */

        /* datepicker */
        gTimepicker = {
            init: function(container){
                if($(container+" .timepicker").length > 0)
                   $(container+" .timepicker").timepicker({showButtonPanel:false});
            }
        }
        gDateTimepicker = {
            init: function(container){
                if($(container+" .datetimepicker").length > 0)
                   $(container+" .datetimepicker").datetimepicker({showButtonPanel:false});
            }
        }
        /* eof datepicker */

        /* Accordion */
        gAccordion = {
            init: function(container){
                if($(container+" .accordion").length > 0){
                   $(container+" .accordion").accordion({heightStyle: "content"});
                   $(container+" .accordion .ui-accordion-header:last").css("border-bottom","0px");
                }
            }
        }
        /* EOF Accordion */

    /* eof jQuery */

    /* bootstrap tooltip */
    gTip = {
        init: function(container){
            if($(container+" .tip").length > 0){
                $(container+" .tip").tooltip({placement: 'top'});
                $(container+" .tipb").tooltip({placement: 'bottom'});
                $(container+" .tipl").tooltip({placement: 'left'});
                $(container+" .tipr").tooltip({placement: 'right'});
            }
        }
    }
    /* eof bootstrap tooltip */

    /* bootstrap popover */
    gPopover = {
        init: function(container){
            $(container+" [data-toggle=popover]").popover();
        }
    }

    /* eof bootstrap popover */

    /* mCustomScrollbar */
    $(".page-navigation").mCustomScrollbar({autoHideScrollbar: true,scrollInertia: 20, advanced: {autoScrollOnFocus: false}});

    gScroll = {
        init: function(container){
            if($(container+" .scroll").length > 0)
               $(container+" .scroll").mCustomScrollbar({autoHideScrollbar: true, advanced: {autoScrollOnFocus: false}});
        }
    }

    $(".modal").on("shown.bs.modal",function(){
        $(this).find(".scroll").mCustomScrollbar("update");
    });
    /* eof mCustomScrollbar */

    /* sparkline */
    gSparkline = {
        init: function(container){
            if($(container+" .sparkline").length > 0)
               $(container+" .sparkline").sparkline('html', { enableTagOptions: true, disableHiddenCheck: true});
        }
    }
    /* eof sparkline */

    /* select2*/
    gSelect2 = {
        init: function(container){
            if($(container+" .select2").length > 0)
               $(container+" .select2").select2();
        }
    }
    /* eof select2*/

    /* tagsinput */
    gTagsInput = {
        init: function(container){
            if($(container+" .tagsinput").length > 0)
               $(container+" .tagsinput").tagsInput({width: '100%',height:'auto'});
        }
    }
    /* eof tagsinput */

    /* icheck */
    gICheck = {
        init: function(container){
            if($(container+" input.icheck").length > 0)
               $(container+" .icheck").iCheck({checkboxClass: 'icheckbox_flat', radioClass: 'iradio_flat'});

            /* IMPORTANT: If you use only default icheck than this part code can be removed from here --> */
            if($(container+" input.icheck-red").length > 0)
                $(container+" .icheck-red").iCheck({checkboxClass: 'icheckbox_flat-red', radioClass: 'iradio_flat-red'});
            if($(container+" input.icheck-grey").length > 0)
                $(container+" .icheck-grey").iCheck({checkboxClass: 'icheckbox_flat-grey', radioClass: 'iradio_flat-grey'});
            if($(container+" input.icheck-blue").length > 0)
                $(container+" .icheck-blue").iCheck({checkboxClass: 'icheckbox_flat-blue', radioClass: 'iradio_flat-blue'});
            if($(container+" input.icheck-orange").length > 0)
                $(container+" .icheck-orange").iCheck({checkboxClass: 'icheckbox_flat-orange', radioClass: 'iradio_flat-orange'});
            if($(container+" input.icheck-pink").length > 0)
                $(container+" .icheck-pink").iCheck({checkboxClass: 'icheckbox_flat-pink', radioClass: 'iradio_flat-pink'});
            if($(container+" input.icheck-purple").length > 0)
                $(container+" .icheck-purple").iCheck({checkboxClass: 'icheckbox_flat-purple', radioClass: 'iradio_flat-purple'});
            if($(container+" input.icheck-yellow").length > 0)
                $(container+" .icheck-yellow").iCheck({checkboxClass: 'icheckbox_flat-yellow', radioClass: 'iradio_flat-yellow'});
            if($(container+" input.icheck-green").length > 0)
                $(container+" .icheck-green").iCheck({checkboxClass: 'icheckbox_flat-green', radioClass: 'iradio_flat-green'});
            if($(container+" input.icheck-aero").length > 0)
                $(container+" .icheck-aero").iCheck({checkboxClass: 'icheckbox_flat-aero', radioClass: 'iradio_flat-aero'});
            /* <-- To Here :) */
        }
    }

    /* eof icheck */

    /* validation */
    gValidate = {
        init: function(container){
            if($(container+" .validate").length > 0)
               $(container+" .validate").validationEngine('attach',{promptPosition : "topLeft"});
        }
    }
    /* eof validation */

    /* Masked Input */
    gMask = {
        init: function(container){
            if($(container+" input[class^='mask_']").length > 0){
                $(container+" input.mask_tin").mask('99-9999999');
                $(container+" input.mask_ssn").mask('999-99-9999');
                $(container+" input.mask_date").mask('9999-99-99');
                $(container+" input.mask_product").mask('a*-999-a999');
                $(container+" input.mask_phone").mask('99 (999) 999-99-99');
                $(container+" input.mask_phone_ext").mask('99 (999) 999-9999? x99999');
                $(container+" input.mask_credit").mask('9999-9999-9999-9999');
                $(container+" input.mask_percent").mask('99%');
            }
        }
    }

    /* EOF Masked Input */

    /* Datatables */
    gTableSimple = {
        init: function(container){
            if($(container+" table.sortable_simple").length > 0)
               $(container+" table.sortable_simple").dataTable({"iDisplayLength": 5,"bLengthChange": false,"bFilter": false,"bInfo": false,"bPaginate": true});
        }
    }
    gTableDefault = {
        init: function(container){
            if($(container+" table.sortable_default").length > 0)
               $(container+" table.sortable_default").dataTable({"iDisplayLength": 5, "sPaginationType": "full_numbers","bLengthChange": false,"bFilter": false,"bInfo": false,"bPaginate": true, "aoColumns": [ { "bSortable": false }, null, null, null, null]});
        }
    }
    gTableSortable = {
        init: function(container){
            if($(container+" table.sortable").length > 0)
               $(container+" table.sortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});
        }
    }
    /* EOF Datatables */

    /* Spinner */
    gSpinner = {
        init: function(container){

            if($(container+" .spinner").length > 0){
                $(container+" .spinner").spinner();
                    /* this samples can be removed */
                    $(container+" .spinner2").spinner({step: 0.1});
                    $(container+" .spinner3").spinner({min: 0,max: 2500,step: 25.15,numberFormat: "C"});
                    /* eof this samples can be removed */

                    $(container+" .ui-spinner").find('span').html('');
            }
        }
    }
    /* eof Spinner */

    /* knob plugin */
    gKnob = {
        init: function(container){
            if($(container+" .knob").length > 0)
               $(container+" .knob input").knob();
        }
    }
    /* eof knob plugin */

    /* Chained select */
    gChained = {
        init: function(container){

            if($(container+" .chained_group").length > 0)
                $(container+" .chained_subgroup").chained(container+" .chained_group");

            if($(container+" .ch_mark").length > 0){
                $(container+" .ch_series").chained(container+" .ch_mark");
                $(container+" .ch_model").chained(container+" .ch_series");
            }

        }


    }

    /* eof Chained select */


    /* My Custom Progressbar */
    $.mpb = function(action,options){

        var settings = $.extend({
            state: '',
            value: [0,0],
            position: '',
            speed: 20,
            complete: null
        },options);

        if(action == 'show' || action == 'update'){

            if(action == 'show'){
                $(".mpb").remove();
                var mpb = '<div class="mpb '+settings.position+'">\n\
                               <div class="mpb-progress'+(settings.state != '' ? ' mpb-'+settings.state: '')+'" style="width:'+settings.value[0]+'%;"></div>\n\
                           </div>';
                $('body').append(mpb);
            }

            var i  = $.isArray(settings.value) ? settings.value[0] : $(".mpb .mpb-progress").width();
            var to = $.isArray(settings.value) ? settings.value[1] : settings.value;

            var timer = setInterval(function(){
                $(".mpb .mpb-progress").css('width',i+'%'); i++;

                if(i > to){
                    clearInterval(timer);
                    if($.isFunction(settings.complete)){
                        settings.complete.call(this);
                    }
                }
            }, settings.speed);

        }

        if(action == 'destroy'){
            $(".mpb").remove();
        }

    }
    /* Eof My Custom Progressbar */

    // new selector case insensivity
     $.expr[':'].containsi = function(a, i, m) {
         return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
     };
    //
    gPlugins("body");
});


function gPlugins(container){
    gDateRangePicker.init(container);
    gSummernote.init(container);
    gSummernoteEditor.init(container);
    gFancybox.init(container);
    gDatepicker.init(container);
    gMDatepicker.init(container);
    gTimepicker.init(container);
    gDateTimepicker.init(container);
    gAccordion.init(container);
    gTip.init(container);
    gPopover.init(container);
    gScroll.init(container);
    gSparkline.init(container);
    gSelect2.init(container);
    gTagsInput.init(container);
    gICheck.init(container);
    gValidate.init(container);
    gMask.init(container);
    gTableSimple.init(container);
    gTableDefault.init(container);
    gTableSortable.init(container);
    gSpinner.init(container);
    gKnob.init(container);
    gChained.init(container);
}




/* file:           actions.js
 * version:        1.3
 * last changed:   23.03.2014
 * description:    In this file you will find all template actions and controllers
*/
$(document).ready(function(){

    $.mpb("show",{value: [0,50],speed: 5});

    navigation_state = navigation_state_was = $(".page-navigation").hasClass('page-navigation-closed');

    $(".page-head-elements .dropdown").on("click",function(event){
        var popup = $(this).next(".popup");

        if(!popup.hasClass('open')){
            popup.addClass('open');
            list_height();
            $(".scroll").mCustomScrollbar("update");
        }else{
            popup.removeClass('open');
        }

        return false;
    });

    /* remove content from ui spinner buttons */
    $(".ui-spinner").find('span').html('');
    /* icons */

    /* table checkall */
    $("table .checkall").on("click", function(){
        var state = $(this).is(":checked") ? true : false;
        var tbody = $(this).parents("table").find("tbody");
        var index = $(this).parent("th").index();

        tbody.find("tr").each(function(){
            $(this).find("td:eq("+index+") input:checkbox").prop("checked",state);
        });
    });
    /* eof table checkall */

    /* page toolbar tabs */
    $(".page-toolbar-tabs a").on("click",function(){
        var pli = $(this).parent("li");
        var act = $($(this).attr("href"));

        $(".page-toolbar-tabs li,.page-toolbar-tab").removeClass("active");
        pli.addClass("active");
        act.addClass("active");

        return false;
    });
    /* eof page toolbar tabs */

    $(".page-head .search").on("click",function(){
        $(this).find("input").focus();
    });

    // navigation sublevels
    $(".navigation > li").each(function(){
        if($(this).children("ul").length > 0){
            $(this).addClass("openable");
        }
    });

    $(".navigation.nav_ci > li > a").on("click",function(){
        $(".navigation.nav_ci > li").removeClass("open");
        $(".navigation.nav_ci > li > ul").slideUp(200);
    });

    $(".navigation li > a").on("click",function(){
        var pli = $(this).parent("li");
        var sub = pli.children("ul");
        if(sub.length > 0){
            sub.is(":visible") ? sub.slideUp(200,function(){
                pli.removeClass("open");
                $(".page-navigation").mCustomScrollbar("update");
            }) : sub.slideDown(200,function(){
                pli.addClass("open");
                $(".page-navigation").mCustomScrollbar("update");
            });
            return false;
        }
    });

    // eof navigation sublevels

    /* input file */
    $(".file .btn,.file input:text").on('click',function(){
        var block = $(this).parents('.file');
        block.find('input:file').click();
        block.find('input:file').change(function(){
            block.find('input:text').val(block.find('input:file').val());
        });
    });
    /* eof input file */

    // List items control show
    gListItemControls = {
        init: function(){
            // on item click
            $(".list .list-item .list-item-content,.list .list-item .list-item-trigger-external").on("click",function(){
                list_item_controls($(this).parents(".list-item"),0);
            });
            // on trigger click
            $(".list .list-item .list-item-trigger").on("click",function(){
                list_item_controls($(this).parents(".list-item"),1);
            });
        }
    }
    gListItemControls.init();
    // eof list items control show

    // Toogle navigation controller
    $(".page-navigation-toggle").on("click",function(){
        page_navigation();
        return false;
    });

    // Toggle sidebar controller
    $(".page-sidebar-toggle").on("click",function(){
        if($(".page-sidebar").hasClass("page-sidebar-opened")){
            //If sidebar opened - close
            page_sidebar('close');
        }else{
            //If sidebar closed - open
            page_sidebar('open');
        }
        return false;
    });
    //End of sidebar controller

    /* block controls */
    $(".block-remove").on("click",function(){
        block_remove($(this).parents(".block"));
        return false;
    });

    $(".block-toggle").on("click",function(){
        block_toggle($(this).parents(".block"));
        return false;
    });

    $(".block-refresh").on("click",function(){
        var block = $(this).parents(".block");
        block_refresh(block);

        setTimeout(function(){
            block_refresh(block);
        },2000);

        return false;
    });
    /* eof block controls */

    /* Draggable blocks */
    if($(".sortableContent").length > 0){
        var scid = 'sc-'+$(".sortableContent").attr('id');

        var sCdata = $.cookies.get( scid );

        if(null != sCdata){
            for(row=0;row<Object.size(sCdata); row++){
                for(column=0;column<Object.size(sCdata[row]);column++){
                    for(block=0;block<Object.size(sCdata[row][column]);block++){
                        $("#"+sCdata[row][column][block]).appendTo(".sortableContent .scRow:eq("+row+") .scCol:eq("+column+")");
                    }
                }
            }
        }

        $(".sortableContent .scCol").sortable({
            connectWith: ".sortableContent .scCol",
            items: "> .block",
            handle: ".block-head",
            placeholder: "scPlaceholder",
            start: function(event,ui){
                $(".scPlaceholder").height(ui.item.height());
            },
            stop: function(event, ui){

                var sorted = {};
                var row = 0;
                $(".sortableContent .scRow").each(function(){
                    sorted[row] = {};
                    $(this).find(".scCol").each(function(){
                        var column = $(this).index();
                        sorted[row][column] = {};

                        $(this).find('.block').each(function(){
                            sorted[row][column][$(this).index()] = $(this).attr('id');
                        });
                    });
                    row++;
                });

                $.cookies.set(scid, JSON.stringify(sorted));
            }
        }).disableSelection();

        $(".sc-set-default").on("click",function(){
            $.cookies.del(scid);
            location.reload();
        });

    }
    /* EOF Draggable blocks */


    // Onload page functions

    $.mpb("update",{value: 100, speed: 5, complete: function(){
        $(".mpb").fadeOut(200,function(){
            $(this).remove();
        });
    }});

});

$(function(){
    page_layout();
    list_controls_wrapper();
    list_height();

    if($.isFunction($.updateCharts)) $.updateCharts();
    if($.isFunction($.updateMaps)) $.updateMaps();
    navigation_state = false;
});

$(document).mouseup(function(e){
    var container = $(".popup");
    if (!container.is(e.target) && container.has(e.target).length === 0)
        container.removeClass('open');
});

$(window).resize(function(){
    // Onresize page functions
    page_layout();
    list_height();
});

function page_layout(){

    if($(window).width() < 769){
        $(".page-container").addClass("page-layout-mobile");
        $(".page-navigation").addClass('page-navigation-closed');
        page_navigation('close');
    }else{
        $(".page-container").removeClass("page-layout-mobile");
        $(".page-navigation").removeClass('page-navigation-closed').css("left","");
        $(".page-container,.page-content").removeAttr("style");


        page_sidebar('close');

        if(navigation_state)
            page_navigation('close');

        if($(".page-container .page-content").height() <= $(".page-navigation").height()-50){
            $(".page-container .page-content").height($(".page-navigation").height()-50);
        }
    }

    $(".page-navigation, .page-sidebar").height($(window).height());
    $(".page-navigation, .page-sidebar").mCustomScrollbar("update");

    if($(".content-wide-control").length > 0)
        $(".content-wide-control").height($(".page-content").height()-$(".page-toolbar").height());

}

function list_item_controls(item,hasTrigger){
    var content     = item.find(".list-item-content");
    var controls    = item.find(".list-item-controls");
    var trigger     = item.find(".list-item-trigger");

    if(trigger.length > 0 && hasTrigger == 0) return false;

    if(!item.hasClass("item-state-active")){
        if(controls.length > 0){

            content.css({position: "absolute"}).animate({left: "-50%"},300);
            controls.animate({right: "0%"},300);

            item.addClass("item-state-active");
        }
    }else{
        content.animate({left: "0%"},300,function(){
            $(this).removeAttr("style");
        });
        controls.animate({right: "-50%"},300,function(){
            item.removeClass("item-state-active");
        });
    }
}

function list_height(){
    $(".list .list-item").each(function(){
        $(this).height($(this).find(".list-item-content").height()+10);
    });
}

function block_remove(block){
    block.animate({'opacity':0},200,function(){
        $(this).remove();
    });
}
function block_toggle(block){
    if(!block.hasClass("block-toggled")){
        block.children("div:not(.block-head)").slideUp(200,function(){
            block.addClass("block-toggled");
            block.find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        });

    }else{
        block.children("div:not(.block-head)").slideDown(200,function(){
            block.removeClass("block-toggled");
            block.find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    }
}
function block_refresh(block){
    if(!block.hasClass("block-refreshing")){
        block.append('<div class="block-refresh-layer"><i class="fa fa-spinner fa-spin"></i></div>');
        block.find(".block-refresh-layer").width(block.width()).height(block.height());
        block.addClass("block-refreshing");
    }else{
        block.find(".block-refresh-layer").remove();
        block.removeClass("block-refreshing");
    }
}

function page_navigation(action,sidebar){
    //Hide sidebar if opened
    if($(".page-sidebar").hasClass("page-sidebar-opened") && !sidebar){
        navigation_state_was = $(".page-navigation").hasClass("page-navigation-closed");
        page_sidebar("close");
        return false;
    }

    //Get width of navigation block
    var navigation_width  = $(".page-navigation").width();
    //Get navigation state
    var navigation_action = null != action ? action : 'auto';
    //Get navigation mode
    var navigation_mode   = $(".page-container").hasClass('page-layout-mobile');

    if(navigation_action == 'open')
        page_navigation_open(navigation_mode,navigation_width);

    if(navigation_action == 'close')
        page_navigation_close(navigation_mode,navigation_width);

    if(navigation_action == 'auto'){
        if(!navigation_mode){
            $(".page-navigation").hasClass("page-navigation-closed")
            ? page_navigation_open(navigation_mode,navigation_width)
            : page_navigation_close(navigation_mode,navigation_width);
        }else{
            !$(".page-navigation").hasClass("page-navigation-opened")
            ? page_navigation_open(navigation_mode,navigation_width)
            : page_navigation_close(navigation_mode,navigation_width);
        }
    }

    return false;
    //End of navigation controller

}

function page_navigation_open(mode,width){

    if(!mode){ //Desktop mode
        $(".page-content,.page-head").animate({'padding-left': width},300,function(){
            $(this).removeAttr("style");
         });
         $(".page-navigation").animate({left: 0},300,function(){
             $(this).removeClass("page-navigation-closed");

             if($.isFunction($.updateCharts)) $.updateCharts();
         });
    }else{ //Mobile mode
        $(".page-container").css({"position":"absolute","width":$(".page-container").width()}).animate({"left": width},300);
        $(".page-navigation").animate({left: 0},300,function(){
            $(this).addClass("page-navigation-opened");
        });
    }

    list_height();

    return false;

}

function page_navigation_close(mode,width){

    if(!mode){ //Desktop mode

        var speed = navigation_state ? 0 : 300;

        $(".page-content,.page-head").animate({"padding-left": 0}, speed);

        $(".page-navigation").animate({left: -width}, speed, function(){
            $(this).addClass("page-navigation-closed");

            if($.isFunction($.updateCharts)) $.updateCharts();
        });

    }else{ //Mobile mode

        $(".page-container").animate({"left": 0},300,function(){
            //$(this).removeAttr("style");
        });

        $(".page-navigation").animate({left: -width},300,function(){
            $(this).removeClass("page-navigation-opened");
        });

    }

    list_height();

    return false;

}

function page_sidebar(action){
    // Get width of sidebar block
    var sidebar_width = 250;
    var navigation_mode = $(".page-container").hasClass('page-layout-mobile');

    //Check state
    if(action == "close"){
        if(!$(".page-sidebar").hasClass("page-sidebar-opened")) return false;

        if(!navigation_state_was && !navigation_mode)
            page_navigation("open",true);

        $(".page-container").animate({left: 0},300);
        $(".page-sidebar").animate({right: -sidebar_width},300,function(){
            $(this).removeClass("page-sidebar-opened");
            page_layout_repair();
        });
    }else{

        navigation_state_was = $(".page-navigation").hasClass("page-navigation-closed");

        //if(!navigation_mode)
            page_navigation("close",true);

        $(".page-container").css("position","absolute").css("width",$(".page-container").width()).animate({left: -sidebar_width},300);
        $(".page-sidebar").animate({right: 0},300,function(){
            $(this).addClass("page-sidebar-opened");
        });
    }
}

function page_layout_repair(){
    if(!$(".page-sidebar").hasClass("page-sidebar-opened")){
        $(".page-container").removeAttr("style");
    }
}

function list_controls_wrapper(){
    $(".list .list-item .list-item-controls, .list .list-item .list-item-right").each(function(){
       $(this).wrapInner("<div/>");
    });
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
