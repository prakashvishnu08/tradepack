# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180711121032) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "zip"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "address_type"
    t.string "name"
    t.string "phone_no"
    t.datetime "deleted_at"
    t.index ["address1"], name: "index_addresses_on_address1"
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
    t.index ["deleted_at"], name: "index_addresses_on_deleted_at"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.string "image"
    t.datetime "deleted_at"
    t.integer "size_id"
    t.index ["ancestry"], name: "index_categories_on_ancestry"
    t.index ["deleted_at"], name: "index_categories_on_deleted_at"
  end

  create_table "category_option_types", force: :cascade do |t|
    t.integer "category_id"
    t.integer "option_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_sellers", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "seller_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_category_sellers_on_category_id"
    t.index ["seller_id"], name: "index_category_sellers_on_seller_id"
  end

  create_table "option_types", force: :cascade do |t|
    t.string "name"
    t.string "presentation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "option_values", force: :cascade do |t|
    t.string "name"
    t.string "presentation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "option_type_id"
    t.string "image"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_option_values_on_deleted_at"
    t.index ["option_type_id"], name: "index_option_values_on_option_type_id"
  end

  create_table "order_values", force: :cascade do |t|
    t.bigint "option_value_id"
    t.bigint "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["option_value_id"], name: "index_order_values_on_option_value_id"
    t.index ["order_id"], name: "index_order_values_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "category_id"
    t.integer "quantity"
    t.boolean "approved", default: false
    t.boolean "accepted", default: false
    t.boolean "canceled", default: false
    t.string "cancelled_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "order_id"
    t.integer "user_id"
    t.integer "seller_id"
    t.decimal "price"
    t.datetime "expires_at"
    t.integer "shipping_id"
    t.integer "billing_id"
    t.string "status"
    t.string "transaction_id"
    t.datetime "purchased_at"
    t.datetime "delivery_date"
    t.text "notification_params"
    t.integer "delay"
    t.boolean "delivered"
    t.integer "pallets"
    t.decimal "total_price"
    t.decimal "delivery_charge"
  end

  create_table "pallets", force: :cascade do |t|
    t.integer "category_id"
    t.integer "seller_id"
    t.integer "option_value_id"
    t.integer "pallet_size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sellers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone_no"
    t.string "vat_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logo"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_sellers_on_deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "role_id"
    t.string "first_name"
    t.string "last_name"
    t.string "company"
    t.string "vat_no"
    t.string "phone_no"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "image"
    t.boolean "approved", default: false
    t.string "vat_code"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
  end

  add_foreign_key "category_sellers", "categories"
  add_foreign_key "category_sellers", "sellers"
  add_foreign_key "option_values", "option_types"
  add_foreign_key "order_values", "option_values"
  add_foreign_key "order_values", "orders"
  add_foreign_key "users", "roles"
end
