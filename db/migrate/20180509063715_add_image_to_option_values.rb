class AddImageToOptionValues < ActiveRecord::Migration[5.1]
  def change
    add_column :option_values, :image, :string
  end
end
