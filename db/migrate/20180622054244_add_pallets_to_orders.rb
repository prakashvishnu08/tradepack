class AddPalletsToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :pallets, :integer
    add_column :orders, :total_price, :decimal
  end
end
