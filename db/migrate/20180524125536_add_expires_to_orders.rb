class AddExpiresToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :expires_at, :datetime
  end
end
