class AddDefaultValueToOrders < ActiveRecord::Migration[5.1]
  def change
    change_column_default :orders, :accepted, from: nil, to: false
    change_column_default :orders, :approved, from: nil, to: false
    change_column_default :orders, :canceled, from: nil, to: false
  end
end
