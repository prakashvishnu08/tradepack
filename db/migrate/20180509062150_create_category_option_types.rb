class CreateCategoryOptionTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :category_option_types do |t|
      t.integer :category_id
      t.integer :option_type_id

      t.timestamps
    end
  end
end
