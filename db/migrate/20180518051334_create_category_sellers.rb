class CreateCategorySellers < ActiveRecord::Migration[5.1]
  def change
    create_table :category_sellers do |t|
      t.references :category, foreign_key: true
      t.references :seller, foreign_key: true

      t.timestamps
    end
  end
end
