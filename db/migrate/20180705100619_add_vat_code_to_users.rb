class AddVatCodeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :vat_code, :string
  end
end
