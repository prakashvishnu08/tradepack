class CreatePallets < ActiveRecord::Migration[5.1]
  def change
    create_table :pallets do |t|
      t.integer :category_id
      t.integer :seller_id
      t.integer :option_value_id
      t.integer :pallet_size

      t.timestamps
    end
  end
end
