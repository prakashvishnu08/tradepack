class AddSizeIdToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :size_id, :integer
  end
end
