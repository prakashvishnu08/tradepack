class AddLogoToSellers < ActiveRecord::Migration[5.1]
  def change
    add_column :sellers, :logo, :string
  end
end
