class CreateOrderValues < ActiveRecord::Migration[5.1]
  def change
    create_table :order_values do |t|
      t.references :option_value, foreign_key: true
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
