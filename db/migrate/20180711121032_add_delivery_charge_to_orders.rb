class AddDeliveryChargeToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :delivery_charge, :decimal
  end
end
