class AddDeliveryDelayToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :delay, :integer
    add_column :orders, :delivered, :boolean
  end
end
