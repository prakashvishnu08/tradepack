class AddOptionTypeToOptionValues < ActiveRecord::Migration[5.1]
  def change
    add_reference :option_values, :option_type, foreign_key: true
  end
end
