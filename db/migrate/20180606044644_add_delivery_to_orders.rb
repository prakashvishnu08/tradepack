class AddDeliveryToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :delivery_date, :datetime
    add_column :orders, :notification_params, :text
  end
end
