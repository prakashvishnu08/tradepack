class RemoveCountryFromSellers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :country_code
    remove_column :sellers, :country
  end
end
