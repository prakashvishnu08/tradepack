class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :category_id
      t.integer :quantity
      t.boolean :approved
      t.boolean :accepted
      t.boolean :canceled
      t.string :cancelled_by

      t.timestamps
    end
  end
end
