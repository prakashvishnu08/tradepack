class OrdersController < ApplicationController
  layout "frontend"
  before_action :authenticate_user!, except: :hook
  skip_before_action :verify_authenticity_token, only: :hook
  # load_and_authorize_resource

  def select_category
    authorize! :select_category, Order, :message => "You are not verified by Tradepack"
    @categories = Category.size_categories
  end

  def new
    if params[:category_id].present?
      @category = Category.find(params[:category_id])
      @sellers = @category.pallet_sellers
      @option_types = @category.option_types
      @count = @option_types.count
      @addresses = current_user.addresses
      @size_id = @category.size_type.id
      @order = Order.new
    else
      redirect_to select_category_orders_path
    end
  end

  def create
    @order = Order.new(order_params)
    @order.option_value_ids = params[:order][:option_value_ids].values
    if @order.save
      flash[:notice] = "Order request is successfully placed"
      redirect_to my_requests_orders_path
    else
      flash[:danger] = "Something went wrong"
      redirect_to select_category_orders_path
    end
  end

  def ask_with_comparator
    @category = Category.find(params[:category_id])
    @seller = Seller.find(params[:seller_id])
    @option_types = @category.option_types
    @count = @option_types.count
    @addresses = current_user.addresses
    @size_id = @category.size_type.id
    @order = Order.new
  end

  def my_requests
    @orders = current_user.orders.includes(:seller, :shipping_address, :category).where(accepted: false).order(created_at: :desc)
  end

  def my_orders
    @orders = current_user.orders.includes(:seller, :shipping_address, :category).where(accepted: true, canceled: false).where.not(transaction_id: nil).order(created_at: :desc)
  end

  def index
    @orders = current_user.orders.where(accepted: false)
  end

  def cancel_order
    @order = Order.find(params[:id])
    @order.update(canceled: true, cancelled_by: "user")
    redirect_to my_requests_orders_path
  end

  def offer_summary
    @order = Order.find(params[:id])
  end

  def hook
    params.permit! # Permit all Paypal input params
    status = params[:payment_status]
    if status == "Completed"
      @order = Order.find(params[:invoice])
      date = Date.today + @order.delay
      @order.update_attributes(notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now, accepted: true, delivery_date: date)
      OrderMailer.order_placed(@order).deliver_now
    end
    render body: nil
  end

  def invoice
    @order = Order.find(params[:id])
    respond_to do |format|
      format.pdf do
        render :pdf => "report"
      end
    end
  end

  def order_summary
    @order = Order.find(params[:id])
  end

  def order_canceled
  end

  def get_pallet
    @category = Category.find(params[:category_id])
    @pallet = Pallet.find_by(category_id: params[:category_id], seller_id: params[:seller_id], option_value_id: params[:value_id])
    if @pallet.present?
      respond_to do |format|
        format.js
      end
    else
      render body: nil
    end
  end

  private

  def order_params
    params.require(:order).permit(:category_id, :quantity, :seller_id, :shipping_id, :pallets, :billing_id, {option_value_ids: []}).merge(user_id: current_user.id)
  end

end

