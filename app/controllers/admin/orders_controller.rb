class Admin::OrdersController < ApplicationController
  before_action :authenticate_admin!

  def index
    @q = Order.ransack(params[:q])
    @orders = @q.result.includes(:user, :category).where(accepted: false).order(created_at: :desc).page(params[:page]).per(20)
  end

  def public_orders
    @q = Order.ransack(params[:q])
    @orders = @q.result.includes(:user, :category).where(approved: true, accepted: true, canceled: false).order(created_at: :desc).page(params[:page]).per(20)
  end

  def show
    @order = Order.find(params[:id])
    @shipping = @order.shipping_address
    @billing = @order.shipping_address
  end

  def paid_order
    @order = Order.find(params[:id])
    @shipping = @order.shipping_address
    @billing = @order.shipping_address
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(order_params)
      flash[:notice] = "Offer notifications sent to user"
      OrderMailer.approved(@order).deliver_now
      redirect_to admin_orders_path
    else
      flash[:notice] = "Something went wrong"
    end
  end

  def cancel_order
    @order = Order.find(params[:id])
    @order.update(canceled: true)
    OrderMailer.canceled(@order).deliver_now
    redirect_to admin_orders_path
  end

  def change_delivery_status
    @order = Order.find(params[:id])
    @order.delivered = !@order.delivered
    @order.save
    respond_to do |format|
      format.js
    end
  end

  def order_params
    params.require(:order).permit(:price, :delay, :total_price, :delivery_charge).merge(approved: true, expires_at: 30.days.from_now)
  end

end
