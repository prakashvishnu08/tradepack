class Admin::SellersController < ApplicationController
  before_action :authenticate_admin!, except: [:states]

  def index
    @sellers = Seller.includes(:address).order(:name).page(params[:page]).per(10)
  end

  def new
    @seller = Seller.new
    @seller.build_address
  end

  def create
    @seller = Seller.new(seller_params)
    if @seller.save
      @seller.categories.each do|cat|
        cat.size_values.each do |value|
          Pallet.create(category_id: cat.id, option_value_id: value.id, seller_id: @seller.id)
        end
      end
      redirect_to admin_sellers_path
    else
      render :new
    end
  end

  def edit
    @seller = Seller.find(params[:id])
    @address = @seller.address
    if @seller.address.country
      country = @seller.address.country
      @country = CS.countries.select{ |x,y| y == country }.first[0]
    else
      @states = {}
    end
  end

  def show
    @seller = Seller.find(params[:id])
    @pallets = @seller.pallets.order(:created_at)
  end

  def update
    @seller = Seller.find(params[:id])
    if @seller.update(seller_params)
      redirect_to admin_sellers_path
    else
      render "edit"
    end
  end

  def load_size_values
    Category.find(params[:ids])
    render body: nil
  end

  def states
    country = params[:country]
    country = CS.countries.select{ |x,y| y == country }.first[0]
    @states = CS.states(country)
    respond_to do |format|
      format.js
    end
  end

  def current_ability
    @current_ability ||= AdminAbility.new(current_admin)
  end

  def destroy
    @seller = Seller.find(params[:id])
    @seller.destroy
    flash[:success] = "Seller deleted"
    redirect_to admin_sellers_path
  end

  private

  def seller_params
    params.require(:seller).permit(:name, :email, :phone_no, :vat_no, :logo, category_ids: [],address_attributes: [:address1, :address2, :city, :state, :country, :zip])
  end

end
