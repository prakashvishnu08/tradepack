class Admin::ProductsController < ApplicationController
  before_action :authenticate_admin!

  def index
    @q = Product.ransack(params[:q])
    @products = @q.result.includes(:category).page(params[:page]).per(10)
    categories
  end

  def new
    @product = Product.new
    #categories
  end

  def create
    @product = Product.new(product_params)
    if @product.save!
      redirect_to admin_products_path
    else
      render "new"
    end
  end

  def edit
    @product = Product.find(params[:id])
    categories
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      redirect_to admin_products_path
    else
      render "edit"
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  private

  def categories
    @categories = Category.ancestry_options(Category.unscoped.arrange(:order => 'title')) {|i| "#{'-' * i.depth} #{i.title.titleize}" }
  end

  def product_params
    params.require(:product).permit(:name, :description, :available_on, :category_id, :code, {seller_ids: [], images: [], option_type_ids: []})
  end

end
