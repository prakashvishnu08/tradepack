class Admin::CategoriesController < ApplicationController
  before_action :authenticate_admin!
  load_and_authorize_resource

  def index
    @categories = Category.all.order(created_at: :desc)
  end

  def new
    @category = Category.new
    #@categories = Category.ancestry_options(Category.unscoped.arrange(:order => 'title')) {|i| "#{'-' * i.depth} #{i.title.titleize}" }
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path
    else
      render "new"
    end
  end

  def edit
    @category = Category.find(params[:id])
    #@categories = Category.ancestry_options((Category.where.not(id: @category.id)).arrange(:order => 'title')) {|i| "#{'-' * i.depth} #{i.title.titleize}" }
  end

  def update
    @category = Category.find(params[:id])
    if @category.update(category_params)
      redirect_to admin_categories_path
    else
      render "edit"
    end
  end

  def load_size_types
    @option_types = OptionType.find(params[:ids])
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    flash[:success] = "Category deleted"
    redirect_to admin_categories_path
  end

  private


  def category_params
    params.require(:category).permit(:title, :image, :size_id, {option_type_ids: []})
  end

end
