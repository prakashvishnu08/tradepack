class Admin::UsersController < ApplicationController
  before_action :authenticate_admin!

  def index
    @q = User.ransack(params[:q])
    @users = @q.result.includes(:primary_address).page(params[:page]).per(10)
  end

  def show
    @user = User.find(params[:id])
    @address = @user.default_address
  end

  def approve
    @user = User.find(params[:id])
    @user.update(approved: true)
    UserMailer.approved(@user).deliver_now
    flash[:notice] = "User approval successfull"
    redirect_to admin_user_path(@user)
  end

end
