class Admin::PalletsController < ApplicationController
  before_action :authenticate_admin!

  def update
    @seller = Seller.find(params[:id])
    params['pallet'].keys.each do |id|
      @pallet = Pallet.find(id.to_i)
      @pallet.update_attributes(pallet_size: params['pallet'][id][:pallet_size])
    end
    flash[:success] = "Pallet size updated"
    redirect_to admin_seller_path(@seller)
  end
end
