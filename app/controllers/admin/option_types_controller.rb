class Admin::OptionTypesController < ApplicationController
  before_action :authenticate_admin!

  def index
    @option_types = OptionType.all.order(created_at: :desc)


  end

  def new
    @option_type = OptionType.new
  end

  def create
    @option_type = OptionType.new(option_type_params)
    if @option_type.save
      redirect_to admin_option_types_path
    else
      render "new"
    end
  end

  def edit
    @option_type = OptionType.find(params[:id])
  end

  def update
    @option_type = OptionType.find(params[:id])
    if @option_type.update(option_type_params)
      redirect_to admin_option_types_path
    else
      render "edit"
    end
  end

  protected

  def option_type_params
    params.require(:option_type).permit(:name, :presentation, option_values_attributes: [:id, :name, :presentation, :image, :_destroy])
  end
end
