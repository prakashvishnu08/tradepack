class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_no_cache
  layout :check_layout

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      # if user_signed_in?
      #   format.html { redirect_to admin_sellers_path, notice: exception.message }
      # elsif admin_signed_in?
        format.html { redirect_to root_path, notice: exception.message }
      # end
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  # def current_ability
  #   if admin_signed_in?
  #     @current_ability ||= Ability.new(current_admin)
  #   else
  #     @current_ability ||= Ability.new(current_user)
  #   end
  # end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :company, :vat_no, :vat_code, :phone_no, :image, primary_address_attributes: [:country, :state, :zip, :city, :address1, :address2]])
    end

    def check_layout
      if devise_controller? && resource.is_a?(Admin)
        "admin_devise"
      elsif devise_controller? && !user_signed_in?
        "user_devise"
      elsif devise_controller? and user_signed_in?
        "frontend"
      end
    end

    def set_no_cache
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "#{1.year.ago}"
    end

    private

    def after_sign_out_path_for(resource_or_scope)
      if resource_or_scope == :user
        root_path
      elsif resource_or_scope == :admin
        new_admin_session_path
      end
    end
end
