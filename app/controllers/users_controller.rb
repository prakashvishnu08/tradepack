class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :new_address, :create_address]
  layout "frontend"

  def edit
    @user
    @addresses = @user.addresses.order(created_at: :desc)
  end

  def update
    if @user.update(edit_params)
      flash[:notice] = "Profile Successfully Updated"
      redirect_to edit_user_path
    else
      render 'edit'
    end
  end

  def edit_address
    @address = Address.find(params[:id])
    country = @address.country
    @country = CS.countries.select{ |x,y| y == country }.first[0]
  end

  def update_address
    @address = Address.find(params[:id])
    if @address.update(address_params)
      flash[:success] = "Address updated"
      redirect_to edit_user_path
    else
      render :edit_address
    end
  end

  def new_address
    @user
    @address = @user.addresses.build
  end

  def create_address
    @user
    @address = @user.addresses.build(address_params)
    if @address.save
      flash[:success] = "Address Crated"
      redirect_to edit_user_path
    else
      render :new_address
    end
  end

  def delete_address
    @address = Address.find(params[:id])
    @address.destroy
    flash[:success] = "Address Deleted"
    redirect_to edit_user_path
  end

  private

  def set_user
    @user = current_user
  end

  def address_params
    params.require(:address).permit(:name, :phone_no, :address1, :address2, :city, :state, :country, :zip)
  end

  def edit_params
    params.require(:user).permit(:first_name, :last_name, :vat_no, :vat_code, :phone_no, :company, :image, addresses_attributes: [:address1, :address2,
      :city, :state, :country, :zip])
  end
end
