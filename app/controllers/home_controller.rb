class HomeController < ApplicationController
  #before_action :authenticate_user!
  layout "frontend"

  def home_page
  end

  def contact_us
    @message = ContactMessage.new
  end

  def send_message
    @message = ContactMessage.new(message_params)
    if @message.valid?
      ContactMailer.contact(@message).deliver_now
      redirect_to home_contact_us_path
      flash[:notice] = "We have received your message and will be in touch soon!"
    else
      flash[:notice] = "There was an error sending your message. Please try again."
      redirect_to home_contact_us_path
    end
  end

  def privacy
  end

  def how_it_works
  end

  def comparator
    authorize! :comparator, Order, :message => "You are not verified by Tradepack"
    @categories = Category.size_categories
    @category = @categories.first
    @sellers = @category.pallet_sellers if @category.present?
  end

  def get_sellers
    @category = Category.find(params[:id])
    @sellers = @category.pallet_sellers if @category.present?
    respond_to do |format|
      format.js
    end
  end

  def faq
  end

  private

  def message_params
    params.require(:contact_message).permit(:name, :email, :body)
  end

end
