module ApplicationHelper

  def user_active_class(*link_path)
    link_path.any? { |x| x == request.env['PATH_INFO'] } ? 'user-links flex1 user-active' : "user-links flex1"
  end

  def check_delivery(order)
    order.delivered? ? "success" : "danger"
  end

  def flash_class_name(key)
    case key
      when 'notice' then 'success'
      when 'alert'  then 'danger'
    else name
    end
  end

  def admin_status(status)
    case status
      when 'Pending' then 'btn button-orange'
      when 'Cancelled' then 'btn btn-danger'
      when 'Approved' then 'btn btn-success'
      when 'Expired' then 'btn btn-primary'
      else "btn btn-default"
    end
  end

end
