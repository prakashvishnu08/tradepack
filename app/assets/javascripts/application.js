// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require rails-ujs
//= require vendor/jquery-1.11.2.min.js
//= require vendor/bootstrap.min.js
//= require jquery-ui.min.js
//= require jquery.mCustomScrollbar.min.js
//= require main.js
//= require soon.min.js
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require bootstrap-select
//= require cocoon
//= require jquery-ui
//= require_tree .

$(document).bind("contextmenu",function(e) {
  e.preventDefault();
});
