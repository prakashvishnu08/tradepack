class ContactMailer < ApplicationMailer
  def contact(message)
    @body = message.body
    @name= message.name
    @email = message.email
    mail(to: "tradepack2018@gmail.com", from: @email, subject: 'Contact Message')
  end
end
