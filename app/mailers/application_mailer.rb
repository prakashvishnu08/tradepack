class ApplicationMailer < ActionMailer::Base
  default from: 'tradepack2018@gmail.com'
  layout 'mailer'
end
