class OrderMailer < ApplicationMailer

  def approved(order)
    @order = order
    @user = order.user
    @email = @user.email
    attachments['offer.pdf'] = WickedPdf.new.pdf_from_string(
      render_to_string(:pdf => "offer", :template => 'admin/orders/offer.pdf.erb')
    )
    mail(:subject => 'Offer Approved', :to => @email) do |format|
      format.html
    end
  end

  def canceled(order)
    @order = order
    @user = order.user
    @email = @user.email
    mail(to: @email, subject: 'Order Canceled')
  end

  def order_placed(order)
    @order = order
    @user = order.user
    @email = @user.email
    attachments['invoice.pdf'] = WickedPdf.new.pdf_from_string(
      render_to_string(:pdf => "Invoice", :template => 'orders/invoice.pdf.erb')
    )
    mail(:subject => 'Order Placed', :to => @email) do |format|
      format.html
    end
  end

end
