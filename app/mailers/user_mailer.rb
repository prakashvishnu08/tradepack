class UserMailer < ApplicationMailer

  def approved(user)
    @user = user
    @email = @user.email
    mail(to: @email, subject: 'Account verified')
  end

end
