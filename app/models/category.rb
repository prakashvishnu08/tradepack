class Category < ApplicationRecord
  acts_as_paranoid
  has_ancestry
  validates :title, :image, :size_type, presence: true
  mount_uploader :image, ImageUploader
  has_many :category_option_types
  has_many :option_types, through: :category_option_types, dependent: :destroy
  has_many :category_sellers
  has_many :sellers, through: :category_sellers, dependent: :destroy
  has_many :orders
  has_many :option_values, through: :option_types
  has_many :pallets
  belongs_to :size_type, class_name: "OptionType", foreign_key: "size_id"
  validates_size_of :image, maximum: 0.5.megabytes.to_i, message: "should be less than 500KB"
  after_update :update_pallets
  before_destroy :delete_pallets

  def to_param
    "#{id}-#{title.parameterize}"
  end

  def size_values
    size_type.option_values
  end

  def pallet_sellers
    sellers.joins(:pallets).where("pallets.pallet_size > ? AND pallets.category_id = ?", 0, self.id).distinct
  end

  def self.size_categories
    Category.joins(:pallets).where("pallets.pallet_size > ?", 0).distinct
  end

  def title_to_upper
    self.title.upcase
  end

  # def self.ancestry_options(items)
  #   result = []
  #   items.map do |item, sub_items|
  #     result << [yield(item), item.id]
  #     #this is a recursive call:
  #     result += ancestry_options(sub_items) {|i| "#{'-' * i.depth} #{i.title.titleize}" }
  #   end
  #   result
  # end

  # def update_pallets(option_type)
  #   self.sellers.each do |seller|
  #     self.size_values.each do |value|
  #       Pallet.find_or_create(seller_id: seller.id, option_value_id: value.id, category_id: self.id)
  #     end
  #   end
  # end

  def update_pallets
    if size_id_changed?
      @pallets = Pallet.where(category_id: self.id)
      @pallets.destroy_all if @pallets.present?
      self.sellers.each do |seller|
        seller.update_pallets(self)
      end
    end
  end
 private

  def delete_pallets
    pallets = Pallet.where(category_id: self.id)
    pallets.destroy_all if pallets.present?
  end

end
