class Address < ApplicationRecord
  acts_as_paranoid
  belongs_to :addressable, polymorphic: true, optional: true
  validates :name, :phone_no, :address1, :address2, :country, :state, :city, :zip, presence: true

  def full_address
    [name, address1, address2, city, state, country, zip, phone_no].compact.join(", ").titleize
  end

  def address_string
    [address1, address2, city, state, country, zip, phone_no].compact.join(", ").titleize
  end


end
