class CategoryOptionType < ApplicationRecord
  belongs_to :category
  belongs_to :option_type
end
