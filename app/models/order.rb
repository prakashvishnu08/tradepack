class Order < ApplicationRecord
  belongs_to :category, -> { with_deleted }
  belongs_to :user
  belongs_to :seller, -> { with_deleted }
  has_many :order_values, dependent: :destroy
  has_many :option_values, through: :order_values
  belongs_to :billing_address, -> { with_deleted }, :class_name => "Address", :foreign_key => "billing_id"
  belongs_to :shipping_address,-> { with_deleted }, :class_name => "Address", :foreign_key => "shipping_id"
  after_save :generate_random_id
  serialize :notification_params
  validates :quantity, presence: true, numericality: {only_integer: true}
  # validates :delay, numericality: { only_integer: true }, on: :update
  # validates :price, numericality: { only_integer: true }, on: :update
  # validates :price, :presence => true, :format => { :with => /^(\$)?(\d+)(\.|,)?\d{0,2}?$/ }

  def paypal_url(return_url, notification_url, cancel_url)
    values = {
     business: 'vishnu.demo@gmail.com',
     cmd: "_xclick",
     upload: 1,
     return: return_url,
     invoice: id,
     currency_code: 'EUR',
     amount: price,
     item_name: order_id,
     quantity: '1',
     cancel_return_url: cancel_url,
     notify_url: notification_url
    }
    # For test transactions use this URL
    "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
    # "https://www.paypal.com/cgi-bin/webscr?&#8221;" + values.to_query
  end

  def self.pre_order_status(status)
    if status == "Approved"
      where(approved: true, canceled: false)
    elsif status == "Cancelled"
      where(canceled: true)
    elsif status == "Pending"
      where(canceled: false, approved: false)
    else
      where("1 = 0")
    end
  end

  def offer_name
    self.order_id.gsub "ORDER", "OFFER"
  end

  def self.ransackable_scopes(auth_object = nil)
    %i(pre_order_status)
  end

  def difference
    if price.present? && total_price.present?
      total_price - price
    else
      ""
    end
  end

  def admin_status
    if self.canceled?
      "Cancelled"
    elsif self.approved?
      "Approved"
    elsif self.expired?
      "Expired"
    else
      "Pending"
    end
  end

  def payment_status
    if self.transaction_id?
      "Paid"
    else
      "Not paid"
    end
  end

  def normal_price
    total_price/total_quantity
  end

  def discount_price
    price/total_quantity
  end

  def expired?
    if self.expires_at && (self.expires_at < Time.now)
      true
    else
      false
    end
  end

  def total_quantity
    quantity*pallets
  end

  def options
    self.option_values.with_deleted.map(&:presentation.downcase).join(", ").titleize
  end

  def delivery_status
    delivered ? "Delivered": "Undelivered"
  end

  private

  def generate_random_id
    return if order_id.present?
    order_id = "ORDER"+"#{rand(9999)}"+"#{self.id}"
    self.update_attributes(order_id: order_id)
  end

end

