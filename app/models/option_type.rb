class OptionType < ApplicationRecord
  has_many :option_values, dependent: :destroy
  has_many :category_option_types, dependent: :destroy
  has_many :categories, through: :category_option_types, dependent: :destroy
  accepts_nested_attributes_for :option_values, allow_destroy: true
  validates :name, :presentation, presence: true
  validates :option_values, length: {minimum: 1, message: 'cannot save, Add at least one option value'}
  has_many :pallet_categories, :foreign_key => "size_id", :class_name => "Category"

  # def delete_pallets(option_value)
  #   Pallet.where(option_value_id)
  # end

  def joined_values
    option_values.pluck(:presentation).join(", ").titleize
  end

  private



end
