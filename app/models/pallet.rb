class Pallet < ApplicationRecord
  validates :seller_id, uniqueness: { scope: [:category_id, :option_value_id] }


  def category
    Category.find(self.category_id)
  end

  def seller
    Seller.find(self.seller_id)
  end

  def option_value
    OptionValue.find(self.option_value_id)
  end

end
