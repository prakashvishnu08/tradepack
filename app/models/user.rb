class User < ApplicationRecord
  before_validation :default_address_values, on: [:create]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  #before_create :set_default_role
  belongs_to :role, required: false
  has_many :addresses, as: :addressable, dependent: :destroy
  has_one :primary_address, -> { where(address_type: "primary") }, as: :addressable, class_name: "Address"
  has_many :orders
  accepts_nested_attributes_for :addresses
  accepts_nested_attributes_for :primary_address
  validates :first_name, :last_name, :phone_no, :company, :vat_no, :phone_no, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates_format_of :phone_no, :with =>  /(?:\+?|\b)[0-9]\b/
  mount_uploader :image, ImageUploader
  validates_size_of :image, maximum: 0.5.megabytes.to_i, message: "should be less than 500KB"
  VAT_COUNTRIES = ['AT','BE','BG','HR','CY','CZ','DK','EE','FI','FR','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','RO','SK','SI','ES','SE','GB']

  def full_name
    [first_name, last_name].join(" ").titleize
  end

  def default_address
    primary_address || addresses.first
  end

  def vat
    [vat_code, vat_no].join.upcase
  end

  private
    def set_default_role
      self.role ||= Role.find_by_name('buyer')
    end

    def default_address_values
      self.primary_address.name = self.first_name
      self.primary_address.phone_no = self.phone_no
    end
end
