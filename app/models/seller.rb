class Seller < ApplicationRecord
  acts_as_paranoid
  before_validation :set_address_fields
  before_destroy :destroy_pallets
  #before_update :update_pallets
  mount_uploader :logo, LogoUploader
  has_and_belongs_to_many :products
  has_one :address, as: :addressable, dependent: :destroy
  has_many :category_sellers
  has_many :categories, through: :category_sellers, dependent: :destroy, after_add: :update_pallets, after_remove: :delete_pallets
  has_one :order
  accepts_nested_attributes_for :address
  has_many :pallets

  validates :email ,:name, :phone_no, :vat_no, :logo, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates_format_of :phone_no, :with =>  /(?:\+?|\b)[0-9]\b/
  validates_size_of :logo, maximum: 0.5.megabytes.to_i, message: "should be less than 500KB"
  #validates :logo, presence: true, on: :create

  def country_name
    country = ISO3166::Country[self.country]
    country.translations[I18n.locale.to_s] || country.name
  end

  def update_pallets(category)
    if !self.new_record?
      category.size_values.each do |value|
        Pallet.find_or_create_by(category_id: category.id, seller_id: self.id, option_value_id: value.id)
      end
    end
  end

  def category_names
    categories.pluck(:title).join(", ").titleize
  end

  def delete_pallets(category)
    if !self.new_record?
      category.size_values.each do |value|
        @pallet = Pallet.find_by(category_id: category.id, seller_id: self.id, option_value_id: value.id)
        @pallet.destroy if @pallet.present?
      end
    end
  end

  private

  def destroy_pallets
    pallets = Pallet.where(seller_id: self.id)
    pallets.destroy_all if pallets.present?
  end

  def set_address_fields
    self.address.name = self.name
    self.address.phone_no = self.phone_no
  end

end
