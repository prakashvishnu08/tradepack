class OptionValue < ApplicationRecord
  after_create_commit :update_pallets
  after_destroy :delete_pallets
  belongs_to :option_type
  has_many :order_values
  has_many :orders, through: :order_values
  validates :name, :presentation, :image, :presentation, presence: true
  mount_uploader :image, ImageUploader
  has_many :pallets
  validates_size_of :image, maximum: 0.5.megabytes.to_i, message: "should be less than 500KB"
  acts_as_paranoid

  private

  def update_pallets
    if self.option_type.pallet_categories.present?
      self.option_type.pallet_categories.each do |cat|
        cat.sellers.each do |seller|
          seller.update_pallets(cat)
        end
      end
    end
  end

  def delete_pallets
    pallets = Pallet.where(option_value_id: self.id)
    pallets.destroy_all if pallets.present?
  end


end
