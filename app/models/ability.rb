class Ability
  include CanCan::Ability

  def initialize(user)
    # if user.is_a?(Admin)
    #   cannot :manage, :all
    # elsif user.is_a?(User)
    #   can :manage, :all
    # end
    if user.is_a?(User)
      if !user.approved?
        can :manage, :all
        cannot :select_category, Order
        cannot :comparator, Order
      else
        can :manage, :all
      end
    else
      can :manage, :all
    end
  end

end

